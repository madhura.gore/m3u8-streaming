// const extractAudio = require('ffmpeg-extract-audio')
 
// convertToAudio();

// async function convertToAudio() {
//     const mp3Stream = await extractAudio({
//         input: 'public/assets/sample1.mp4',
//         output: 'output1.mp3'
//     });
// }
var ffmpeg = require('fluent-ffmpeg');
const m3u8stream = require('m3u8stream');
console.log(ffmpeg);

/**
 *    input - string, path of input file
 *    output - string, path of output file
 *    callback - function, node-style callback fn (error, result)        
 */
function convert(input, output, callback) {
    ffmpeg(input)
        .output(output)
        .on('end', function() {                    
            console.log('conversion ended');
            callback(null);
        }).on('error', function(err){
            console.log('error: ', err.code, err.msg);
            callback(err);
        }).run();
}

convert('./sample1.mp4', './output1.mp3', function(err){
   if(!err) {
       console.log('conversion complete');
       //...

   }
});