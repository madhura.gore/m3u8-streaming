const express = require('express')
const fs = require('fs')
const path = require('path')
const app = express()
const request = require('request');
const vtt = require('vtt-creator');

app.use(express.static(path.join(__dirname, 'public')))
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.htm'))
})

app.get('/captions',function(req, res, next){

  fs.readFile('./public/assets/json/transcription.json',function(err,data){
    const jsonData = JSON.parse(data.toString());
    let words = Object.keys(jsonData.words).map(function(key) {
      return jsonData.words[key];
    });
    let startTime = Object.keys(jsonData.start_time).map(function(key) {
      return Number(jsonData.start_time[key]);
    });
    let endTime = Object.keys(jsonData.end_time).map(function(key) {
      return Number(jsonData.end_time[key]);
    });
 
    let sentences = "";
    let subtitles = [];
    for(let i=0;i<words.length ;i++){
      let obj = {};
      obj["word"] = words[i];
      obj["startTime"] = startTime[i];
      obj["endTime"] = endTime[i];
      subtitles.push(obj);
    }

    let vttObj = new vtt();
    let count = 0 ;
    for(let i=0;i<subtitles.length; i++){
      var start,end;
      if(count == 0){
        start = subtitles[i].startTime;
        count++;
      }
      if(count<6){
        sentences += " "+subtitles[i].word;
        count++;
      }
      if(count == 6){
        end = subtitles[i].endTime;
        vttObj.add(Number(start),Number(end),sentences);
        sentences = "";
        count = 0;
      }

    }
    
    const vttData = vttObj.toString();
    fs.writeFile(path.join(__dirname,"./public/assets/json/sample.vtt"), vttData, function(err) {
      if(err) {
          return console.log(err);
      }
      var filePath = path.join(__dirname, './public/assets/json/sample.vtt');
      var stat = fs.statSync(filePath);

      

      res.writeHead(200, {
          'Content-Type': 'text/vtt',
          'Content-Length': stat.size
      });

      var readStream = fs.createReadStream(filePath);
      // We replaced all the event handlers with a simple call to readStream.pipe()
      return readStream.pipe(res);
        }); 
  }) 
});

app.get('/video', function(req, res, next) {
  const path = './public/assets/news1.mp4'
  const stat = fs.statSync(path)
  const fileSize = stat.size
  const range = req.headers.range

  if (range) {
    const parts = range.replace(/bytes=/, "").split("-")
    const start = parseInt(parts[0], 10)
    const end = parts[1]
      ? parseInt(parts[1], 10)
      : fileSize-1

    const chunksize = (end-start)+1
    const file = fs.createReadStream(path, {start, end})
    const head = {
      'Content-Range': `bytes ${start}-${end}/${fileSize}`,
      'Accept-Ranges': 'bytes',
      'Content-Length': chunksize,
      'Content-Type': 'video/mp4',
    }

    res.writeHead(206, head)
    file.pipe(res)
  } else {
    const head = {
      'Content-Length': fileSize,
      'Content-Type': 'video/mp4',
    }
    res.writeHead(200, head)
    fs.createReadStream(path).pipe(res)
  }
})

app.listen(3000, function () {
  console.log('Listening on port 3000!')
})